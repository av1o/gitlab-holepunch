package gitlab

import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.dcas.dev/open-source/gitlab-holepunch/pkg/conf"
	"strings"
)

type Service struct {
	client    *gitlab.Client
	secret    string
	approvals int
}

// NewService creates the GitLab client
func NewService(baseUrl, token string, secret string, approvals int) (*Service, error) {
	c, err := gitlab.NewClient(token, gitlab.WithBaseURL(baseUrl))
	if err != nil {
		return nil, err
	}
	log.Infof("created new gitlab client")
	s := new(Service)
	s.client = c
	s.secret = secret
	s.approvals = approvals

	return s, nil
}

func (svc *Service) GetCurrentUser() (user *gitlab.User, err error) {
	user, _, err = svc.client.Users.CurrentUser()
	return
}

func (svc *Service) CreateCommentWebhook(pId int) error {
	if ok, err := svc.hasWebhook(pId); err == nil && ok {
		log.Infof("project %d has note webhook setup", pId)
		return nil
	}
	log.Infof("creating note webhook for %d", pId)
	t := true
	opts := &gitlab.AddProjectHookOptions{
		URL:                   &conf.Config.URL,
		NoteEvents:            &t,
		EnableSSLVerification: &t,
		Token:                 &svc.secret,
	}
	_, _, err := svc.client.Projects.AddProjectHook(pId, opts)
	return err
}

func (svc *Service) hasWebhook(pId int) (bool, error) {
	hooks, _, err := svc.client.Projects.ListProjectHooks(pId, nil)
	if err != nil {
		return false, err
	}
	for _, h := range hooks {
		if h.URL == conf.Config.URL && h.NoteEvents {
			return true, nil
		}
	}
	return false, errors.New("couldn't find webhook")
}

func (svc *Service) getMessage(mr *gitlab.MergeEvent, message *string, users []*gitlab.ProjectMember) string {
	title := *message
	if len(users) > 0 {
		log.Infof("tagging %d users in mr %d", len(users), mr.ObjectAttributes.IID)
		msg := "Please comment 'lgtm' to indicate your approval.\n"
		for _, m := range users {
			msg += " @" + m.Username
		}
		title = msg
	}
	return title
}

func (svc *Service) DiscussionExists(pID int, mrId int, uid int) (*gitlab.Discussion, error) {
	disc, _, err := svc.client.Discussions.ListMergeRequestDiscussions(pID, mrId, nil)
	if err != nil {
		return nil, err
	}
	for _, d := range disc {
		for _, n := range d.Notes {
			if n.Author.ID == uid {
				log.Debugf("found note we created")
				if strings.HasPrefix(n.Body, "Please comment") || n.Body == conf.Config.Message {
					return d, nil
				}
			}
		}
	}
	return nil, errors.New("couldn't find our previous note")
}

func (svc *Service) CreateDiscussion(mr *gitlab.MergeEvent, message *string, users []*gitlab.ProjectMember) error {
	msg := svc.getMessage(mr, message, users)

	log.Infof("creating discussion for project %d, mr %d", mr.Project.ID, mr.ObjectAttributes.IID)
	opts := &gitlab.CreateMergeRequestDiscussionOptions{
		Body: &msg,
	}
	_, _, err := svc.client.Discussions.CreateMergeRequestDiscussion(mr.Project.ID, mr.ObjectAttributes.IID, opts)
	return err
}

func (svc *Service) getLabelNames(labels []gitlab.Label) []string {
	names := make([]string, len(labels))
	for i, l := range labels {
		names[i] = l.Name
	}
	return names
}

/**
* Updates metadata for a target MR
* Adds the "needs review" label
 */
func (svc *Service) UpdateMR(event *gitlab.MergeEvent) error {
	log.Infof("updating mr %d in project %d to meet requirements", event.ObjectAttributes.IID, event.Project.ID)
	newLabels := gitlab.Labels{
		"needs review",
		fmt.Sprintf("needs %d", svc.approvals),
	}
	// append the existing labels
	newLabels = append(newLabels, svc.getLabelNames(event.Labels)...)
	// create the updated options object
	opts := gitlab.UpdateMergeRequestOptions{
		Labels: newLabels,
	}
	// opts.AssigneeID = &event.User.ID // assign to the user who triggered the event
	_, _, err := svc.client.MergeRequests.UpdateMergeRequest(event.Project.ID, event.ObjectAttributes.IID, &opts)
	return err
}
