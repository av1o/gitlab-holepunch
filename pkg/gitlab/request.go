package gitlab

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.dcas.dev/open-source/gitlab-holepunch/pkg/conf"
	"io/ioutil"
	"net/http"
	"regexp"
)

var nsMatcher *regexp.Regexp

func GetCommentPayload(r *http.Request) (*gitlab.MergeCommentEvent, error) {
	// read the body
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Errorf("failed to read request body %s", err)
		return nil, err
	}

	// unmarshall the data
	var payload gitlab.MergeCommentEvent
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Errorf("failed to unmarshall payload %s", err)
		return nil, err
	}
	log.Debug("successfully unmarshalled payload")
	return &payload, nil
}

func GetMergePayload(r *http.Request) (*gitlab.MergeEvent, error) {
	// read the body
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Errorf("failed to read request body %s", err)
		return nil, err
	}

	// unmarshall the data
	var payload gitlab.MergeEvent
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Errorf("failed to unmarshall payload %s", err)
		return nil, err
	}
	log.Debug("successfully unmarshalled payload")
	return &payload, nil
}

/**
Determines whether the payload can be processed
*/
func IsMrEvent(p *gitlab.MergeEvent) bool {
	if p.ObjectKind != "merge_request" {
		log.Infof("skipping request with kind %s (want merge_request)", p.ObjectKind)
		return false
	}
	if p.ObjectAttributes.State != "opened" {
		log.Infof("skipping request with state %s (want opened)", p.ObjectAttributes.State)
		return false
	}
	// only comment when the MR is initially opened
	if p.ObjectAttributes.Action != "open" {
		log.Infof("skipping request with action %s (want open)", p.ObjectAttributes.Action)
		return false
	}
	// build the regex matcher
	if nsMatcher == nil {
		var err error
		nsMatcher, err = regexp.Compile(conf.Config.NsRegex)
		if err != nil {
			log.Errorf("failed to create regex '%s' - all namespaces will be allowed", conf.Config.NsRegex)
			return true
		}
	}
	matches := nsMatcher.MatchString(p.Project.PathWithNamespace)
	log.Debugf("regex matches for namespace %s %v", p.Project.PathWithNamespace, matches)
	return matches
}
