package gitlab

import (
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.dcas.dev/open-source/gitlab-holepunch/pkg/conf"
	"net/http"
)

type MRService struct {
	gs *Service
}

func NewMRService(gs *Service) *MRService {
	ms := new(MRService)
	ms.gs = gs

	return ms
}

func (ms *MRService) HandleMergeEvent(r *http.Request, uid int) error {
	payload, err := GetMergePayload(r)
	if err != nil {
		return err
	}
	// check if this payload is relevant to us
	if IsMrEvent(payload) {
		log.Debug("extracted payload, will create discussion")
		err = ms.gs.CreateCommentWebhook(payload.Project.ID)
		if err != nil {
			log.Warnf("failed to create note webhook for project %d", payload.Project.ID)
		}
		t := true // ?
		// update project settings
		log.Infof("ensuring project %d settings are compliant", payload.Project.ID)
		opts := &gitlab.EditProjectOptions{
			//OnlyAllowMergeIfPipelineSucceeds:          &t,
			OnlyAllowMergeIfAllDiscussionsAreResolved: &t,
			RemoveSourceBranchAfterMerge:              &t,
		}
		_, _, err = ms.gs.client.Projects.EditProject(payload.Project.ID, opts)
		if err != nil {
			log.Warnf("failed to patch project settings: %s", err)
		}

		// create the discussion thread
		users, err := ms.gs.GetUsersForProject(payload.Project.ID, 0, 10)
		if err != nil {
			log.Warnf("failed to load users for project %d", payload.Project.ID)
		}
		if conf.Config.CreateMessage {
			// create the discussion only if one doesn't already exist
			if _, err := ms.gs.DiscussionExists(payload.Project.ID, payload.ObjectAttributes.IID, uid); err != nil {
				if err := ms.gs.CreateDiscussion(payload, &conf.Config.Message, users); err != nil {
					log.WithError(err).Error("failed to create discussion")
					return err
				}
			}
		}
		return ms.gs.UpdateMR(payload)
	}
	return nil
}
