package gitlab

import (
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"net/http"
	"strconv"
	"strings"
)

type CommentService struct {
	gs          *Service
	approvals   int
	selfApprove bool
}

func NewCommentService(gs *Service, approvals int, selfApprove bool) *CommentService {
	cs := new(CommentService)
	cs.gs = gs
	cs.approvals = approvals
	cs.selfApprove = selfApprove

	return cs
}

func (cs *CommentService) HandleCommentEvent(r *http.Request, uid int) error {
	payload, err := GetCommentPayload(r)
	if err != nil {
		return err
	}
	if payload.ObjectAttributes.NoteableType != "MergeRequest" {
		log.Debugf("skipping comment event with unacceptable type %s", payload.ObjectAttributes.NoteableType)
		return nil
	}
	// get the original discussion thread
	disco, err := cs.gs.DiscussionExists(payload.ProjectID, payload.MergeRequest.IID, uid)
	if err != nil {
		log.Errorf("failed to find our original comment for mr %d", payload.MergeRequest.IID)
		return err
	}
	// get the notes on the MR
	notes, err := cs.getMRNotes(payload)
	if err != nil {
		log.Errorf("failed to get notes for mr %d", payload.MergeRequest.IID)
		return err
	}
	votes := make(map[string]int)
	// collate all the 'lgtm' votes
	for _, n := range notes {
		if n.Body == "lgtm" {
			// check whether the assignee is allowed to approve
			if cs.selfApprove || n.Author.ID != payload.MergeRequest.AssigneeID {
				votes[n.Author.Username]++
			}
		}
	}
	required := cs.getRequiredApprovals(payload)
	log.Debugf("found votes for mr %d: %v", payload.MergeRequest.IID, votes)
	log.Infof("got votes for %d (%d/%d)", payload.MergeRequest.IID, len(votes), required)
	if len(votes) >= required {
		log.Infof("got positive number of votes, will approve %d", payload.MergeRequest.IID)
		resolved := true
		opts := &gitlab.ResolveMergeRequestDiscussionOptions{
			Resolved: &resolved,
		}
		// resolve the MR discussion we created
		_, _, err = cs.gs.client.Discussions.ResolveMergeRequestDiscussion(payload.ProjectID, payload.MergeRequest.IID, disco.ID, opts)
		return err
	}
	return nil
}

func (cs *CommentService) getRequiredApprovals(p *gitlab.MergeCommentEvent) int {
	mr, _, err := cs.gs.client.MergeRequests.GetMergeRequest(p.ProjectID, p.MergeRequest.IID, nil)
	if err != nil {
		log.Errorf("failed to get mr %d/%d: %s", p.ProjectID, p.MergeRequest.IID, err)
		return cs.approvals
	}
	// can't have less than 1 approval
	max := 1
	for _, label := range mr.Labels {
		l := strings.ToLower(label)
		// check labels which look like 'needs 2', 'needs 1'
		if strings.HasPrefix(l, "needs ") {
			numS := strings.Trim(l, "needs ")
			num, err := strconv.Atoi(numS)
			if err != nil {
				log.Errorf("failed to extract approval count from label: '%s' (%s)", l, numS)
			}
			// find the biggest number
			if num > max {
				max = num
			}
		}
	}
	return max
}

func (cs *CommentService) getMRNotes(mr *gitlab.MergeCommentEvent) (notes []*gitlab.Note, err error) {
	notes, _, err = cs.gs.client.Notes.ListMergeRequestNotes(mr.ProjectID, mr.MergeRequest.IID, nil)
	return
}
