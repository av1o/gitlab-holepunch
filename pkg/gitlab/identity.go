package gitlab

import (
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

/**
Gets a list of users for a given project
Returns an empty list on error
*/
func (svc *Service) GetUsersForProject(pID int, page int, perPage int) ([]*gitlab.ProjectMember, error) {
	log.Debugf("getting users for project %d", pID)

	userMap := make(map[int]*gitlab.ProjectMember)

	opts := &gitlab.ListProjectMembersOptions{
		ListOptions: gitlab.ListOptions{
			Page:    page,
			PerPage: perPage,
		},
	}
	// get the 1st x users who are direct members
	users, _, err := svc.client.ProjectMembers.ListProjectMembers(pID, opts)
	if err != nil {
		return nil, err
	}
	for _, u := range users {
		userMap[u.ID] = u
	}
	// if we didn't get enough direct users, lets get users from parent groups
	if len(userMap) < perPage {
		// get users who are ancestors
		opts.PerPage *= 2
		users, _, err = svc.client.ProjectMembers.ListAllProjectMembers(pID, opts)
		if err != nil {
			return nil, err
		}
		for _, u := range users {
			userMap[u.ID] = u
		}
	}
	// convert the map back to a slice
	var u []*gitlab.ProjectMember
	idx := 0
	for _, v := range userMap {
		// don't go over size
		if idx < perPage {
			u = append(u, v)
		}
		idx++
	}
	return u, nil
}
