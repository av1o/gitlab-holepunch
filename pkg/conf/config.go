package conf

// Deprecated
type GlobalConfig struct {
	Message       string
	CreateMessage bool
	NsRegex       string
	URL           string
}

// Deprecated
var Config = GlobalConfig{}
