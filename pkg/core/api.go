package core

import (
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/gitlab-holepunch/pkg/gitlab"
	"net/http"
)

type CreationOpts struct {
	Approvals   int
	SelfApprove bool
}

type API struct {
	gitlabService  *gitlab.Service
	commentService *gitlab.CommentService
	mrService      *gitlab.MRService
	secretKey      string
	hasSecret      bool
	uid            int
}

func NewAPI(secretKey string, gs *gitlab.Service, opts *CreationOpts) (*API, error) {
	a := new(API)
	a.secretKey = secretKey
	a.hasSecret = len(secretKey) > 0
	a.gitlabService = gs
	a.commentService = gitlab.NewCommentService(gs, opts.Approvals, opts.SelfApprove)
	a.mrService = gitlab.NewMRService(gs)

	// verify the connection
	user, err := gs.GetCurrentUser()
	if err != nil {
		log.WithError(err).Error("failed to determine our GitLab user")
		return nil, err
	}
	log.Infof("running as %d (%s)", user.ID, user.Username)
	a.uid = user.ID

	return a, nil
}

func (a *API) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	requestID := tracer.GetRequestId(r)
	if !a.shouldAllowRequest(r) {
		// throw 403 if the incorrect secret was sent
		log.WithField("id", requestID).Warnf("rejecting request from %s", r.UserAgent())
		http.Error(w, "403 Forbidden", http.StatusForbidden)
		return
	}
	err := a.processEvent(r)
	if err != nil {
		log.WithError(err).WithField("id", requestID).Error("failed to process event")
		http.Error(w, "failed to process event", http.StatusBadRequest)
		return
	}
	// return 200
	_, _ = w.Write([]byte("OK"))
}

func (a *API) shouldAllowRequest(r *http.Request) bool {
	if a.hasSecret {
		sentSecret := r.Header.Get("X-GitLab-Token")
		if sentSecret != a.secretKey {
			return false
		}
	}
	return true
}

func (a *API) processEvent(r *http.Request) error {
	requestID := tracer.GetRequestId(r)
	event := r.Header.Get("X-GitLab-Event")
	log.WithField("id", requestID).Debugf("found incoming event: '%s'", event)
	var err error

	switch event {
	case "Note Hook":
		err = a.commentService.HandleCommentEvent(r, a.uid)
	case "System Hook":
		fallthrough
	case "Merge Request Hook":
		err = a.mrService.HandleMergeEvent(r, a.uid)
	default:
		err = fmt.Errorf("unsupported event '%s'", event)
	}
	return err
}
