package core

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestAPI_shouldAllowRequest(t *testing.T) {
	api := API{secretKey: "hunter2", hasSecret: true}
	request := &http.Request{Header: http.Header{}}
	request.Header.Add("X-GitLab-Token", "hunter2")

	assert.True(t, api.shouldAllowRequest(request))
}

func TestAPI_shouldAllowRequestBadToken(t *testing.T) {
	api := API{secretKey: "hunter2", hasSecret: true}
	request := &http.Request{Header: http.Header{}}
	request.Header.Add("X-GitLab-Token", "password1")

	assert.False(t, api.shouldAllowRequest(request))
}
