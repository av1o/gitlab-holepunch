# Holepunch

This project aims to act as a somewhat-workaround for the lack of MR approvals in GitLab CE.

### How does it work

When a merge request gets created, GitLab sends a webhook to Holepunch.
Holepunch then uses the GitLab API to create a thread on the MR. This thread must be resolved in order to merge the request.

### Setup

`Settings -> General -> Merge Requests`

Check `All discussions must be resolved`

`Settings -> General -> Webhooks`

Create a webhook:

* `URL` - url to Holepunch
* Set a secret token *(optional)*
* Check only `Merge request events`

### Configuration

Holepunch has a number of arguments which can be specified.
This can be applied directly or via environment variables.

E.g. `./holepunch -port=8080` or `PORT=8080 holepunch`

`token` - gitlab API token **(required)**

`debug` - if set, enables debug logging

`port` - set the port to run on (default: `8080`)

`url` - url of the gitlab instance (default: `https://gitlab.com`)

`secret` - webhook secret (optional)

`message` - comment left on new MRs (default: `Resolve this thread to indicate your approval of this MR.`)

`create_message` - whether to create an MR comment (default: `true`)