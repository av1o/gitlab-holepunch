module gitlab.dcas.dev/open-source/gitlab-holepunch

go 1.15

require (
	github.com/djcass44/go-tracer v0.2.0
	github.com/namsral/flag v1.7.4-pre
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.4.0
	github.com/xanzy/go-gitlab v0.40.2
)
