package main

import (
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/namsral/flag"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/gitlab-holepunch/pkg/conf"
	"gitlab.dcas.dev/open-source/gitlab-holepunch/pkg/core"
	"gitlab.dcas.dev/open-source/gitlab-holepunch/pkg/gitlab"
	"net/http"
	"os"
)

func main() {
	// log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)

	debug := flag.Bool("debug", false, "enables debug logging")
	port := flag.Int("port", 8080, "port to run on")
	baseURL := flag.String("url", "https://gitlab.com", "url of the gitlab instance")
	token := flag.String("token", "", "auth token")
	secret := flag.String("secret", "", "secret token sent by GitLab")
	flag.StringVar(&conf.Config.Message, "message", "Resolve this thread to indicate your approval of this MR.", "content of the MR comment")
	flag.BoolVar(&conf.Config.CreateMessage, "create_message", true, "whether to create an MR comment")
	flag.StringVar(&conf.Config.NsRegex, "ns_regex", ".*", "regex for enabled namespaces")
	flag.StringVar(&conf.Config.URL, "self-url", "http://localhost:8080", "url that holepunch can be reached by GitLab")
	selfApprove := flag.Bool("allow-self-approvals", false, "allow the assigned user to approve their own MR")
	approvals := flag.Int("approvals", 1, "number of approvals required")

	flag.Parse()

	// enable debug logging if asked
	if *debug {
		log.SetLevel(log.DebugLevel)
		log.Debug("enabling debug output")
	} else {
		log.SetLevel(log.InfoLevel)
	}

	if len(*token) == 0 {
		log.Fatalf("token must be set")
	}
	if len(*secret) == 0 {
		log.Warnf("no secret token has been specified")
	}

	log.Infof("using self url %s", conf.Config.URL)
	log.Infof("approval config: {required: %d, allow-self: %v}", *approvals, *selfApprove)

	gitlabService, err := gitlab.NewService(*baseURL, *token, *secret, *approvals)
	// bail out if the client couldn't be setup
	if err != nil {
		log.WithError(err).Fatal("failed to create GitLab client, cannot continue")
	}

	api, err := core.NewAPI(*secret, gitlabService, &core.CreationOpts{
		Approvals:   *approvals,
		SelfApprove: *selfApprove,
	})
	if err != nil {
		log.WithError(err).Fatal("failed to create api client, cannot continue")
	}

	// ping endpoint for health checking
	http.HandleFunc("/ping", func(writer http.ResponseWriter, request *http.Request) {
		log.Debugf("ping from %s", request.UserAgent())
		_, _ = writer.Write([]byte("OK"))
	})
	http.Handle("/", tracer.NewHandler(api))

	// start the server
	addr := fmt.Sprintf(":%d", *port)
	log.Infof("starting server on interface %s", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
